import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ColorState{
  Color color;

  ColorState({this.color});
}

class ColorCubit extends Cubit<ColorState>{
  ColorCubit() : super(ColorState (color: Colors.red));

  void setRed(){
    emit(ColorState(color: Colors.red));
  }
  void setGreen(){
    emit(ColorState(color: Colors.green));
  }
  void setBlue(){
    emit(ColorState(color: Colors.blue));
  }
}