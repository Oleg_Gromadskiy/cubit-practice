import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_cubit/state/color_state.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: BlocProvider(
        create: (_) => ColorCubit(),
        child: Builder(
          builder: (ctx) => Scaffold(
            drawer: Drawer(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    onPressed: () => ctx.read<ColorCubit>().setRed(),
                    color: Colors.red,
                  ),
                  RaisedButton(
                    onPressed: () => ctx.read<ColorCubit>().setGreen(),
                    color: Colors.green,
                  ),
                  RaisedButton(
                    onPressed: () => ctx.read<ColorCubit>().setBlue(),
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
            appBar: AppBar(
              title: Text('Material App Bar'),
            ),
            body: SizedBox.expand(
              child: BlocBuilder<ColorCubit, ColorState>(
                builder: (_, state) => ColoredBox(
                  color: state.color,
                ),
              )
            ),
          ),
        ),
      ),
    );
  }
}
